/**
 * Created by Ryan on 8/15/2016.
 */


// Set up login services
Meteor.startup(function() {
    // Add Facebook configuration entry
    ServiceConfiguration.configurations.update(
        { "service": "facebook" },
        {
            $set: {
                "appId": "235101160218976",
                "secret": "4dbcea966f5aeeb181b7b8573631d307"
            }
        },
        { upsert: true }
    );

});

// TODO replace with api files
import '../../api/users/server/publications.js';
// import '../../api/lists/methods.js';
// import '../../api/lists/server/publications.js';
// import '../../api/todos/methods.js';
// import '../../api/todos/server/publications.js';
