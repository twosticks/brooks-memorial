import './app-body.html';

import {Meteor} from 'meteor/meteor';
import {ReactiveVar} from 'meteor/reactive-var';
import {ReactiveDict} from 'meteor/reactive-dict';
// import {Lists} from '../../api/lists/lists.js';
// import {Websites} from '../../api/websites/websites.js';
import {Template} from 'meteor/templating';
import {ActiveRoute} from 'meteor/zimme:active-route';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {TAPi18n} from 'meteor/tap:i18n';

// import {insert} from '../../api/lists/methods.js';
// import {siteInsert} from '../../api/websites/methods.js';

import graph from 'fbgraph';
import { setAccessToken } from 'fbgraph';


// import '../components/loading.js';

const CONNECTION_ISSUE_TIMEOUT = 5000;

// A store which is local to this file?
const showConnectionIssue = new ReactiveVar(false);

Meteor.startup(() => {
    // Only show the connection error box if it has been 5 seconds since
    // the app started
    setTimeout(() => {
        // FIXME:
        // Launch screen handle created in lib/router.js
        // dataReadyHold.release();

        // Show the connection error box
        showConnectionIssue.set(true);
    }, CONNECTION_ISSUE_TIMEOUT);
});

Template.App_body.helpers({
    externalData:() => {
        // Given a userId, get the user's Facebook access token


    }
});

Template.App_body.events({
    //add your events here
});

Template.App_body.onCreated(function appBodyOnCreated() {
    //add your statement here
    this.subscribe("user.services");
});

Template.App_body.onRendered(function appBodyOnRendered() {
    //add your statement here
});

Template.App_body.onDestroyed(function appBodyOnDestroyed() {
    //add your statement here
});

