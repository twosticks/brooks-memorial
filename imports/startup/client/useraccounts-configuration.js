/**
 * Created by Ryan on 8/15/2016.
 */
import { AccountsTemplates } from 'meteor/useraccounts:core';
import { Accounts } from 'meteor/accounts-base';
import { TAPi18n } from 'meteor/tap:i18n';


AccountsTemplates.configure({
    forbidClientAccountCreation: true,
    showForgotPasswordLink: false,
    texts: {
        errors: {
            loginForbidden: TAPi18n.__('Incorrect username or password'),
            pwdMismatch: TAPi18n.__('Passwords don\'t match'),
        },
        title: {
            signIn: TAPi18n.__('Sign In'),
            signUp: TAPi18n.__('Join'),
        },
    },
    defaultTemplate: 'Auth_page',
    defaultLayout: 'App_body',
    defaultContentRegion: 'main',
    defaultLayoutRegions: {},
});

// ServiceConfiguration.configurations.upsert(
//     { service: "facebook" },
//     {
//         $set: {
//             appId: "1292962797",
//             loginStyle: "popup",
//             secret: "4dbcea966f5aeeb181b7b8573631d307"
//         }
//     }
// );
Accounts.ui.config({
    requestPermissions: {
        facebook: ['public_profile', 'user_education_history','email']
    }
});
Meteor.loginWithFacebook({
    requestPermissions: ['public_profile', 'user_education_history','email']
}, function (err) {
    if (err)
        Session.set('errorMessage', err.reason || 'Unknown error');
});