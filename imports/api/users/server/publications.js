/**
 * Created by Ryan on 8/15/2016.
 */
import { Meteor } from 'meteor/meteor';


/*
* const user = Meteor.users.findOne(Meteor.userId());
 const fbAccessToken = user.services.facebook.accessToken;
 graph.setAccessToken(fbAccessToken);
 graph.setVersion("2.7");
 let options = {
 headers: {

 }
 };
 graph.setOptions(options).get("/me", function(err, res) {
 console.log(res); // { id: '4', name: 'Mark Zuckerberg'... }
 });
 // return console.log(fbAccessToken);
* */


Meteor.publish("user.services", function userInfo(){
    return Meteor.users.find({},{ $fields: { services: 1} });
});